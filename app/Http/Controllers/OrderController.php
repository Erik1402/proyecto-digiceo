<?php

namespace App\Http\Controllers;

use App\Services\CartService;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    
    public $cartService;

    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
        //verificamos si hay un usuario protegiendo la ruta
        $this->middleware('auth');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cart = $this->cartService->getFromCookie();

        if(!isset($cart) || $cart->products->isEmpty()){
            return redirect()
            ->back()
            ->withErrors("El carrito esta vacio");
        }

        return view('orders.create')->with([
            'cart' => $cart,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     //creamos una orden y la asociamos al usuario
    public function store(Request $request)
    {
         $user = $request->user();

        $order = $user->orders()->create([
            'status' => 'pending',
        ]);
            //ahora pasamos la orden de la cookie a la BD, el cart viene del cartservice
            $cart = $this->cartService->getFromCookie();

            //construimos la lista de id con sus variables, la asignamos en la variable cartproduct
            //luego el elemento element va mapeando los productos
            $cartProductsWithQuantity = $cart->products->mapWithKeys(function ($product){
                $element[$product->id] = ['quantity' => $product->pivot->quantity];

                return $element;
            });


            $order->products()->attach($cartProductsWithQuantity->toArray());

            return redirect()->route('orders.payments.create', ['order' => $order]);
    }

}
