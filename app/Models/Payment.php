<?php

namespace App\Models;

use App\Models\Order; //importamos el modelo de Order
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount',
        'payed_at',
        'order_id', //agregamos el order_id
    ];

    /**
    * Los atributos serán mutados a fechas 
    * @var array
    */
    protected $dates = ['payed_at'];

    // Traemos la clase de order para la relacion pertenece a 
    public function order(){
        return $this->belongsTo(Order::class);
    }
}
