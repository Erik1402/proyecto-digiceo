<div class="card">
    <img class="card-img-top" height="300" src="{{ asset($product->images->first()->path) }}">
    <div class="card-body">
        <h4 class="text-right"><strong>${{$product->price}}</strong></h5>
        <h5 class="card-title ">{{$product->title}}</h5>
            <p class="card-text">{{$product->description}}</p>
            <p class="card-text"><strong>{{$product->stock}} Disponibles</strong></p>
            {{-- si estamos en el carrito se mostrara este boton, de lo contrario el otro --}}
            @if (isset($cart))
            <span class="text-center mb-4">{{ $product->pivot->quantity }} en tu carrito (${{ $product->total }})</span>
            <form class="d-inline" method="POST" action="{{ route('products.carts.destroy', ['cart' => $cart->id, 'product' => $product->id]) }}">
                @csrf
                @method('DELETE')
                <button class="btn btn-warning" type="submit">Eliminar del carrito</button>
            </form>
            @else 
            <form class="d-inline" method="POST" action="{{ route('products.carts.store', ['product' => $product->id]) }}">
                @csrf
                <button class="btn btn-success" type="submit">Agregar al carrito</button>
            </form>
            @endif
    </div>
    
</div>
