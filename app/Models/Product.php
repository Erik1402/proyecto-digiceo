<?php

namespace App\Models;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Image;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'description', 
        'price', 
        'stock', 
        'status',
    ];

    public function carts()
    {
        //relacion polimorfica de muchos a muchos
        //cuando se accede la clase producto que nos traiga el elemento quantity con Withpivot
        return $this->morphedByMany(Cart::class, 'productable')->withPivot('quantity');
    }
    public function orders()
    {
        //cuando se accede la clase producto que nos traiga el elemento quantity con Withpivot
        return $this->morphedByMany(Order::class, 'productable')->withPivot('quantity');
    }

    //Un producto tiene muchas imagenes, lo usamos a traves de morphToMany
    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    //Scope para productos disponibles
     public function scopeAvailable($query)
     {
         $query->where('status', 'available');
     }

    //Funcion para calcular el total de una orden
     public function getTotalAttribute()
     {
         return $this->pivot->quantity * $this->price;
     }
    
}
