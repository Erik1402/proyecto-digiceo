@extends('layouts.master')
@section('content')

@section('breadcrumb')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Products</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>
            <h4 class="page-title">Detalles de la orden</h4>
            <h5 class="text-center mb-4"><strong>Total de la orden: {{ $cart->total}} </strong></h5>

            <div class="text-center mb-3">
                <form class="d-inline" method="POST" action="{{ route('orders.store') }}">
                    @csrf
                    <button class="btn btn-success" type="submit">Confirmar orden</button>
                </form>
            </div>
            
        </div>
    </div>
</div>
<!-- end page title -->
@endsection

    

    <div class="table-responsive">
        <table class="table table-striped">
            <thead class="thead-light">
            <tr>
                <th>
                   Producto
                </th>
                <th>
                    Precio
                </th>
                <th>
                    Cantidad
                </th>
                <th>Total</th>
               
            </tr>    
            <thead>
                <tbody>
                    @foreach ($cart->products as $product)
                    <tr>

                        <td>
                            <img width="100" src="{{ asset($product->images->first()->path) }}">
                            {{ $product->title }}</td>
                        <td>{{ $product->price }}</td>
                        <td>{{ $product->pivot->quantity }}</td>
                        <td>
                            <strong>
                                ${{ $product->total }}
                            </strong>
                        </td>
                       

                    </tr>
                    @endforeach
                </tbody>
        </table>
    </div>
    <script type="text/javascript">
        function confirmarEliminar() {
            var x = confirm("¿Deseas eliminar este producto?");
            if (x)
                return true;
            else
                return false;
        }
        </script>
@endsection

