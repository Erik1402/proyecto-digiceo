<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Order; //importamos la clase de Order de su modelo
use App\Models\Payment; //importamos la clase de Payment de su modelo
use App\Models\Image; //importamos la clase de Payment de su modelo

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'admin_since',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

      /**
    * Los atributos serán mutados a fechas 
    * @var array
    */
    protected $dates = ['admin_since'];

    //creamos la funcion de ordenes, un usuario tiene muchas ordenes
    //especificamos el nombre de la clase foranea en caso de que laravel no la encuentre
    public function orders()
    {
        return $this->hasMany(Order::class, 'customer_id');
    }

    //desde user acceder al customer_id  atra ves de payment 
    public function payments()
    {
        return $this->hasManyThrough(Payment::class, Order::class, 'customer_id');
    }

    //Relacion polimorfica, un usuario tiene una imagen
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}
