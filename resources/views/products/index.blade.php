@extends('layouts.master')
@section('content')

@section('breadcrumb')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Products</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>
            <h4 class="page-title">List of products</h4>
        </div>
    </div>
</div>
<!-- end page title -->
@endsection


<a class="btn btn-success mb-4" href="{{route('products.create')}}">Create a product</a>

    @empty ($products)
    {{-- @if (empty($products)) --}}
<div class="alert alert-warning">
    The list of products is empty
</div>
    @else
    

    <div class="table-responsive">
        <table class="table table-striped">
            <thead class="thead-light">
            <tr>
                <th>
                    Id
                </th>
                <th>
                    Title
                </th>
                <th>
                    Description
                </th>
                <th>Price</th>
                <th>Stock</th>
                <th>Status</th>
                <th>Actions</th>
               
            </tr>    
            <thead>
                <tbody>
                    @foreach ($products as $product)
                    <tr>
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->title }}</td>
                        <td>{{ $product->description }}</td>
                        <td>${{ $product->price }}</td>
                        <td>{{ $product->stock }}</td>
                        <td>{{ $product->status }}</td>
                        <td>
                            {{-- A través de ID o a través de titulo --}}
                            <a btn btn-link href="{{ route('products.show', ['product' => $product -> id])}}"><i class="mdi mdi-eye"></i></a>
                            {{-- <a btn btn-link href="{{ route('products.show', ['product' => $product -> title])}}"><i class="mdi mdi-eye"></i></a> --}}
                            <a btn btn-link href="{{ route('products.edit', ['product' => $product -> id])}}"><i class="mdi mdi-pencil"></i></a>
                            <form class="d-inline" method="POST" onsubmit="return confirmarEliminar()" action="{{ route('products.destroy',
                            ['product' => $product->id]) }}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-link"><i class="mdi mdi-delete"></i></button>


                            </form>
                        
                        </td>

                    </tr>
                    @endforeach
                </tbody>
        </table>
    </div>
    <script type="text/javascript">
        function confirmarEliminar() {
            var x = confirm("¿Deseas eliminar este producto?");
            if (x)
                return true;
            else
                return false;
        }
        </script>
    @endempty
@endsection

