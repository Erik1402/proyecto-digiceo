<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('auth/login');
});

Route::get('/pages-login', function () {
    return view('auth/login');
});




Route::group(['middleware' => 'auth'], function () {
    Route::get('pages-logout', 'DashboardController@logout');
    Route::get('home', 'DashboardController@index');

    /*Esta línea resuelve todas las otras rutas del CRUD*/
    Route::resource('products', 'ProductController');

    /* Route::get('products', 'ProductController@index')->name('products.index');
    Route::get('products/create', 'ProductController@create')->name('products.create');
    Route::post('products/create', 'ProductController@store')->name('products.store');
    Route::get('products/{product}', 'ProductController@show')->name('products.show'); */
    /*Así lo traemos por el titulo en lugar del id, tambien se modifica la vista */
    /* Route::get('products/{product:title}', 'ProductController@show')->name('products.show'); */
    /* Route::get('products/{product}/edit', 'ProductController@edit')->name('products.edit');
    Route::match(['put','patch'], 'products/{product}', 'ProductController@update')->name('products.update');
    Route::delete('products/{product}', 'ProductController@destroy')->name('products.destroy'); */
    Route::resource('products.carts', 'ProductCartController')->only(['store', 'destroy']);

    /*Para carts */
    Route::resource('carts', 'CartController')->only('index');
    /*Para orders */
    Route::resource('orders', 'OrderController')->only(['create', 'store']);
    /*para procesar la orden*/
    Route::resource('orders.payments', 'OrderPaymentController')->only(['create', 'store']);

});

