<?php

namespace App\Services;
use App\Models\Cart;
use Illuminate\Support\Facades\Cookie;

class CartService
{
    protected $cookieName = 'cart';

    public function getFromCookie()
    {
        $cartId = Cookie::get($this->cookieName);

        $cart = Cart::find($cartId);

        return $cart;
    }

    public function getFromCookieOrCreate()
    {
        //si es una instancia valida se va a retornar, si no es una instancia valida se crea una nueva
        $cart = $this -> getFromCookie(); 

        return $cart ?? Cart::create();
    }

    public function makeCookie(Cart $cart)
    {
        return Cookie::make($this->cookieName, $cart->id, 7 * 24 * 60);
    }

    //El contador
    public function countProducts()
    {
        $cart = $this->getFromCookie();
            //si carrito es diferente de null
        if ($cart != null ) {
            //aqui se aplican las colecciones a traves de pluck llamando pivot y el campo quantity, luego lo sumamos
            return $cart->products->pluck('pivot.quantity')->sum();
        } 

        return 0;
        
    }
}