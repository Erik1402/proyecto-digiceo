@extends('layouts.master')
@section('content')

@section('breadcrumb')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Products</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>
            <h4 class="page-title">Detalles del pago</h4>
            <h5 class="text-center mb-4"><strong>Total de la orden: {{ $order->total}} </strong></h5>

            <div class="text-center mb-3">
                <form class="d-inline" method="POST" action="{{ route('orders.payments.store', ['order' => $order->id]) }}">
                    @csrf
                    <button class="btn btn-success" type="submit">Pagar</button>
                </form>
            </div>
            
        </div>
    </div>
</div>
<!-- end page title -->
@endsection

@endsection

