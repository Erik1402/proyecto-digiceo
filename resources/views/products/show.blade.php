
@extends('layouts.master')
@section('breadcrumb')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="{{route('products.index')}}">Products</a></li>
                    <li class="breadcrumb-item active">{{$product->title}}</li>
                </ol>
            </div>
            <h4 class="page-title">Product details</h4>
        </div>
    </div>
</div>
<!-- end page title -->
@endsection
@section('content')
@include('components.product-card')

    @endsection