@extends('layouts.master')

@section('css')

@endsection

@section('breadcrumb')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Minton</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>
            <h4 class="page-title">Tu carrito</h4>
        </div>
    </div>
</div>
<!-- end page title -->
@endsection

@section('content')
<div class="row">
    <div class="col">
        <div class="card-box">
           {{--  si no esta establecido cart  o si el contador de productos es vacio --}}
            @if (!isset($cart) || $cart->products->isEmpty())
                <div class="alert alert-warning">
                    Tu carrito esta vacio
                </div>
            @else
            <h4 class="text-center mb-4"><strong>Total: {{ $cart->total }}</strong></h4>
            <a class="btn btn-success mb-3" href="{{ route('orders.create') }}">Empezar orden</a>
            <div class="row">
                @foreach ($cart->products as $product) 
                <div class="col-3">
                 @include('components.product-card')
                </div>
                @endforeach
            </div>
             
            @endempty
            
        </div>
    </div><!-- end col -->
</div>
@endsection

@section('script')

@endsection

@section('script-bottom')
<!-- init js -->

@endsection