<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    public function products()
    {
        //cuando se accede la clase producto que nos traiga el elemento quantity con Withpivot
        return $this->morphToMany(Product::class, 'productable')->withPivot('quantity');
    }

    //Funcion para calcular el totales, utilizando el pluck
    public function getTotalAttribute()
    {
        return $this->products->pluck('total')->sum();
    }
}
