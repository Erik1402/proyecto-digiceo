
@extends('layouts.master')
@section('breadcrumb')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="{{route('products.index')}}">Products</a></li>
                    <li class="breadcrumb-item active">{{$product->title}}</li>
                </ol>
            </div>
            <h4 class="page-title">Edit a product</h4>
        </div>
    </div>
</div>
<!-- end page title -->
@endsection
@section('content')

<h5>Edit a product</h5>
    <form method="POST" action="{{ route('products.update', ['product' => $product -> id])}}">
        @csrf
        @method('PUT')
        <div class="form-row">
            <label>Titulo</label>
            <input class="form-control" type="text" name="title" value="{{ $product->title}}" required>
            
        </div>
        <div class="form-row">
            <label>Descripcion</label>
            <input class="form-control" type="text" name="description" value="{{ $product->description}}" required>
        </div>
        <div class="form-row">
            <label>Price</label>
            <input class="form-control" type="number" min="1.00" step="0.01" name="price" value="{{ $product->price}}" required>
            
        </div>
        <div class="form-row">
            <label>Stock</label>
            <input class="form-control" type="number" min="0" name="stock" value="{{ $product->stock}}" required>
            
        </div>
        <div class="form-row">
            <label>Status</label>
            <select class="custom-select" name="status"  >
                <option disabled selected value="{{ $product->sotck}}">{{ $product->status}}</option>
                <option value="available">available</option>
                <option value="unavalaible">unavalaible</option>
                {{-- <option {{ $product->status == 'available' ? 'selected' : '' }} value="available">available</option>
                <option {{ $product->status == 'unavalaible' ? 'selected' : '' }} value="unavalaible" selected>unavalaible</option> --}}
            </select>
            
        </div>
        <div class="form-row mt-3">
         <button type="submit" class="btn btn-primary btn-lg">Edit Product</button>
            
        </div>
    </form>

  
@endsection

