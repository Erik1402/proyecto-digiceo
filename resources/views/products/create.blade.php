@extends('layouts.master')
@section('breadcrumb')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="{{route('products.index')}}">Products</a></li>
                   
                </ol>
            </div>
            <h4 class="page-title">Create a product</h4>
        </div>
    </div>
</div>
<!-- end page title -->
@endsection
@section('content')

    <form method="POST" action="{{ route('products.store')}}">
        @csrf
        <div class="form-row">
            <label>Titulo</label>
            <input class="form-control" type="text" name="title" required>
            
        </div>
        <div class="form-row">
            <label>Descripcion</label>
            <input class="form-control" type="text" name="description" required>
        </div>
        <div class="form-row">
            <label>Price</label>
            <input class="form-control" type="number" min="1.00" step="0.01" name="price" required>
            
        </div>
        <div class="form-row">
            <label>Stock</label>
            <input class="form-control" type="number" min="0" name="stock" required>
            
        </div>
        <div class="form-row">
            <label>Status</label>
            <select class="custom-select" name="status" required>
                <option value="" selected>Select...</option>
                <option value="available">available</option>
                <option value="unavalaible" selected>unavalaible</option>
            </select>
            
        </div>
        <div class="form-row mt-3">
         <button type="submit" class="btn btn-primary btn-lg">Create Product</button>
            
        </div>
    </form>

    @endsection
