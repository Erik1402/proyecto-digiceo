-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-03-2021 a las 00:28:45
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `digiceo-bd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_03_24_181019_create_product', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) UNSIGNED NOT NULL,
  `stock` int(10) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unavailable',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `title`, `description`, `price`, `stock`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Et debitis.', 'Qui dolores voluptas neque rem debitis.', 3.49, 8, 'available', '2021-03-25 00:48:58', '2021-03-25 02:18:24'),
(2, 'Qui illo omnis.', 'Non cupiditate et ut consectetur. Et ab voluptate et non eos nihil.', 47.06, 10, 'unavailable', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(3, 'Repellendus qui amet.', 'Quo vel est ut est et dolor.', 38.70, 6, 'unavailable', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(4, 'Debitis qui nesciunt.', 'Aut omnis modi laboriosam architecto illum autem. Quaerat quo sint atque dolor.', 75.81, 3, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(5, 'Est fugiat.', 'Nulla dignissimos aliquam ab reprehenderit rerum porro. Iusto eius voluptas quia quasi non.', 96.10, 6, 'unavailable', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(6, 'Et molestiae unde.', 'Necessitatibus non temporibus nulla nulla. Accusamus rerum maxime illo deserunt ad quasi.', 22.41, 6, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(7, 'Eum tenetur ipsam.', 'Eligendi et est vitae iure. Quam totam aut voluptatem ipsam aut beatae qui numquam.', 89.61, 8, 'unavailable', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(8, 'Et eligendi.', 'Explicabo fuga reprehenderit voluptatem tempora officia. Eaque ab quos voluptate omnis assumenda minus at.', 65.05, 5, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(9, 'Odio culpa ipsum.', 'Maiores tempore voluptates magnam ipsam repellendus.', 78.95, 10, 'unavailable', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(10, 'Aut quasi quia.', 'Exercitationem eum dicta nisi consectetur voluptas dolor temporibus.', 12.58, 9, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(11, 'Sequi maxime harum.', 'Deleniti voluptates atque beatae in id placeat veritatis reprehenderit.', 11.24, 2, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(12, 'Ullam excepturi exercitationem.', 'Voluptas quibusdam dicta odit et qui. Commodi repellendus hic est ut eum.', 5.09, 5, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(13, 'Animi laudantium.', 'Mollitia beatae enim ipsum harum sequi quibusdam.', 28.85, 9, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(14, 'Aliquid qui.', 'Sapiente quis quia eaque nostrum corporis fuga aut. Ut aperiam vel dolore voluptas.', 32.20, 9, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(15, 'Et aut neque blanditiis nam.', 'Ab nobis tempore debitis voluptatem et velit.', 61.09, 4, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(16, 'Reprehenderit nobis est.', 'Et omnis quos totam. Inventore necessitatibus suscipit amet placeat.', 8.45, 3, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(17, 'Temporibus excepturi.', 'Velit sed iure voluptate veritatis et. Aut accusamus beatae molestiae veritatis consequatur ipsa.', 22.27, 2, 'unavailable', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(18, 'Deserunt consequatur iure quaerat.', 'Et at debitis nihil et accusantium. Incidunt quis tempora odit quam quia sit.', 20.70, 3, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(19, 'Voluptate quo asperiores.', 'Esse est et et excepturi consequatur qui.', 38.26, 6, 'unavailable', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(20, 'Nobis accusamus nihil quis.', 'Nostrum est quisquam totam est iusto earum veniam. Dolorum vel incidunt aut quis doloribus repudiandae.', 83.33, 3, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(21, 'Voluptas quia explicabo omnis.', 'Temporibus ratione et rem veritatis dolor corporis incidunt.', 67.87, 7, 'unavailable', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(22, 'Enim pariatur suscipit modi.', 'Facere harum id sint. Vel illo quis alias recusandae quo eaque sit.', 98.79, 6, 'unavailable', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(23, 'Qui dolores porro natus.', 'Facilis libero vitae et explicabo et.', 61.72, 2, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(24, 'Eligendi officia vel.', 'Quo sed officia voluptatibus et amet deleniti.', 36.38, 6, 'unavailable', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(25, 'Ut quis maiores incidunt.', 'Aut possimus qui nemo velit repellat sunt natus. Autem sunt ex et voluptas placeat.', 84.25, 1, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(26, 'Dolore laboriosam.', 'Distinctio omnis dolorum iusto magnam est vero reiciendis magni.', 63.70, 2, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(27, 'Et nisi eos.', 'Sapiente est dolorem dignissimos.', 21.95, 2, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(28, 'Est consequatur autem.', 'Sed repellat dignissimos quia nam. Voluptas nostrum odit incidunt vero.', 29.55, 6, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(29, 'Id id similique velit aliquid.', 'Natus dolorem et dolor aut reprehenderit repellendus ea.', 68.55, 4, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(30, 'Tempore praesentium iusto laudantium.', 'Totam temporibus adipisci esse quia voluptates eos.', 85.94, 8, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(31, 'Accusantium corporis quo.', 'Quia ab quia consequatur fuga corrupti. Magnam culpa est ea in quos illum hic.', 91.61, 9, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(32, 'Velit eos libero.', 'Consequuntur aliquam aut rerum ratione nulla sint magnam. Quibusdam quasi inventore porro praesentium.', 32.68, 5, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(33, 'Reprehenderit rerum quod consectetur.', 'Aut ducimus ea accusantium officia vel delectus. Natus autem animi corporis in voluptatem quaerat cum.', 32.67, 7, 'unavailable', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(34, 'Incidunt perferendis omnis.', 'Dolores architecto impedit ratione itaque. Quibusdam inventore officia at facere.', 9.15, 8, 'unavailable', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(35, 'Minima quidem alias.', 'Eum iure asperiores quaerat enim deleniti ducimus minus.', 3.05, 8, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(36, 'Deserunt ut tempora.', 'Fuga illum et ea. Provident rerum quis consequatur earum.', 80.34, 1, 'unavailable', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(37, 'Reprehenderit itaque.', 'Esse odit provident itaque aut eum enim.', 21.69, 4, 'unavailable', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(38, 'Delectus molestiae ut.', 'Nemo autem excepturi asperiores provident. Aperiam et quibusdam minima nam amet et.', 44.68, 4, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(39, 'Rerum praesentium iure commodi.', 'Laudantium earum soluta hic dolorum nihil delectus.', 85.67, 2, 'unavailable', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(40, 'Consequatur et beatae.', 'Dolorem distinctio adipisci numquam facere. Id provident officiis maxime excepturi repudiandae officia ipsam.', 58.85, 4, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(41, 'Quod minima delectus.', 'Aut excepturi aut iure quam ut natus.', 46.24, 5, 'unavailable', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(42, 'Exercitationem aut illum quia.', 'Natus quo accusamus tenetur eveniet qui. Ut nam vero debitis.', 93.07, 5, 'unavailable', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(43, 'Dolor repellat est.', 'Quod vitae velit et natus voluptatem.', 26.88, 3, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(44, 'Nulla sit qui.', 'Aliquam ducimus enim alias praesentium id aut. Mollitia neque qui autem quibusdam odit vel.', 78.90, 4, 'unavailable', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(45, 'Facere sit deleniti.', 'Porro repellendus dolores est necessitatibus rem sit enim provident. Impedit sed qui natus expedita nisi.', 70.22, 10, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(46, 'Non libero maxime sit.', 'Repellendus delectus aperiam reprehenderit voluptas nulla consectetur. Nihil libero enim unde delectus voluptas provident.', 12.92, 8, 'unavailable', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(47, 'Quam fugiat inventore repellat.', 'Iusto et aliquam aut qui.', 23.60, 1, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58'),
(48, 'Odio sunt omnis rerum.', 'Qui molestiae fuga magni nam aliquid nam et et. Ut minima harum architecto corporis sit aut.', 3.82, 9, 'available', '2021-03-25 00:48:58', '2021-03-25 00:48:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Erik Romero', 'erik@dc-digiceo.com', NULL, '$2y$10$ZB8uuy9oXOCdBTtIIuw5pug1Rkc8c/IsQjuiABCXPcXvJArkhDDqq', NULL, '2021-03-25 01:01:13', '2021-03-25 01:01:13');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
