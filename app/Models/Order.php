<?php

namespace App\Models;

use App\Models\Payment; //importamos la clase Payment del modelo
use App\Models\User; //importamos la clase de User de su modelo
use App\Models\Product; //importamos la definicion de product
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status',
        'customer_id', //agregar el customer_id
    ];

    // Relacionamos el pago con, una order tiene un pago, Order tiene un payment
    public function payment()
    {
        return $this->hasOne(Payment::class);
    }

    /*Relacionamos el order con el usuario, order pertenece a un usuario
    tambien indicamos el nombre de la clave foranea en caso de errores, en este caso indicamos
    que es customer_id para identificarla*/
    public function user(){
        return $this->belongsTo(User::class, 'customer_id');
    }

    public function products()
    {
        //cuando se accede la clase producto que nos traiga el elemento quantity con Withpivot
        return $this->morphToMany(Product::class, 'productable')->withPivot('quantity');
    }

      //Funcion para calcular el totales
      public function getTotalAttribute()
      {
          return $this->products->pluck('total')->sum();
      }
}
